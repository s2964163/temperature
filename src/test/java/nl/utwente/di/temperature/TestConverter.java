package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

/**
 * Tests the Converter
 */
public class TestConverter {
    @Test
    public void testBook1() {
        Converter converter = new Converter();
        double temp = converter.getTemperature(0);
        Assertions.assertEquals(32.0,temp, 0.0, "0 Celsius to fahrenheit");
    }
}
