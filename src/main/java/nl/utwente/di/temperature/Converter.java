package nl.utwente.di.temperature;


public class Converter {

    public double getTemperature(double temp) {
        return (temp * 9 / 5) + 32;
    }
}
