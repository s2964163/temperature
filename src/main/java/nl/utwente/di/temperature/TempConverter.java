package nl.utwente.di.temperature;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TempConverter extends HttpServlet {
  /**
   * 
   */
  @Serial
  private static final long serialVersionUID = 1L;
  private Converter converter;

  public void init() {
    converter = new Converter();
  }

  public void doGet(HttpServletRequest request,
      HttpServletResponse response)
      throws IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Temperature Converter";

    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    String doc = """
        <!DOCTYPE HTML>
        <HTML>
        <HEAD><TITLE>""" + title +
        """
            </TITLE><LINK REL=STYLESHEET HREF="styles.css"></HEAD>
            <BODY BGCOLOR="#FDF5E6">
            <H1>""" +
        title +
        "</H1>\n" +
        "  <p>Temperature (\u00b0C): " +
        request.getParameter("temp") +
        "\u00b0</p>" +
        "<p>Temperature (\u00b0F): " +
        converter.getTemperature(Double.parseDouble(request.getParameter("temp"))) +
        "\u00b0</p></BODY></HTML>";
    out.println(doc);
  }
}
